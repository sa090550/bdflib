About this project
==================

Important things to know
about ``bdflib`` and its development.

Requirements
------------

Python 2.7,
Python 3.5 or greater
(earlier version may work, but they are unsupported)

``bdflib`` does not require anything outside the standard library.

License
-------

``bdflib`` is published under the GNU General Public License version 3.

Development
-----------

The source-code and issue tracker for ``bdflib`` are at
https://gitlab.com/Screwtapello/bdflib

Once you've checked out the ``bdflib`` source code,
you can run the ``bdflib`` unit tests with:

.. code-block:: console

    $ python3 -m unittest

Alternatively,
if you have `tox <https://pypi.org/project/tox/>`_ installed,
you can just run:

.. code-block:: console

    $ tox

to run all the tests and build all the documentation.

Further reading
---------------

- `Bitmap Distribution Format version 2.1
  <https://www.x.org/docs/BDF/bdf.pdf>`_ (PDF format)
  is the X11 version of the BDF specification,
  based on the Adobe standard.
- `Glyph Bitmap Distribution Format (BDF) Specification version 2.2
  <http://partners.adobe.com/public/developer/en/font/5005.BDF_Spec.pdf>`_
  (PDF format)
  is the Adobe version of the BDF specification.
  Version 2.2 is backward-compatible with version 2.1,
  and adds extra features to support scripts with vertical text layout.
  ``bdflib`` does not implement this version,
  but some things are better explained here than in the X11 version.
- `X Logical Font Description Conventions
  <https://www.x.org/docs/XLFD/xlfd.pdf>`_
  describes the extra font properties that X11 uses,
  above and beyond the basic BDF specification.
- `BDF Fonts and Modern Linux
  <https://thristian.livejournal.com/90017.html>`_
  describes practical concerns with making a BDF font
  that will be properly recognised by a modern Linux system.
